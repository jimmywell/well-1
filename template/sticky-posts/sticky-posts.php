<?php
/**
 * Posts Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'n2t-block-posts-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'n2t-block-posts';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$args = [
    'post_type' => 'post',
    'post__in' => get_option( 'sticky_posts' ),
    'posts_per_page' => 3,
    'ignore_sticky_posts' => 1
];
if(is_category()){
    $args['cat'] = get_queried_object()->term_id;
}
$posts = get_posts($args);

if(!empty($posts)):
?>
<section id="spotlight_1-0" class="comp spotlight-full spotlight" style="position: relative;">
    <div class="spotlight-blocks">
        <?php if( isset($posts[0]) && !empty($posts[0]) ): ?>
        <a id="block_1-0" class="comp block--large block" href="<?php echo get_permalink($posts[0]->ID); ?>">
            <div class="block__media">
                <div class="img-placeholder" style="padding-bottom:60.0%;">
                    <?php echo get_the_post_thumbnail($posts[0]->ID, 'full', ['class' => 'block__img']); ?>
                </div>
            </div>
            <div class="block__content" data-kicker="Fitness and Nutrition News">
                <div class="block__title">
                    <span><?php echo get_the_title($posts[0]->ID); ?></span>
                </div>
                <div class="block__byline"><?php the_field('by_line', $posts[0]->ID); ?></div>
            </div>
        </a>
        <?php endif; ?>
        <div class="spotlight-secondary">
            <?php if( isset($posts[1]) && !empty($posts[1]) ): ?>
            <a id="block_2-0" class="comp block" rel="" data-doc-id="" href="<?php echo get_permalink($posts[1]->ID); ?>">
                <div class="block__media">
                    <div class="img-placeholder" style="padding-bottom:66.6%;">
                        <?php echo get_the_post_thumbnail($posts[1]->ID, 'large', ['class' => 'block__img']); ?>
                    </div>
                </div>
                <div class="block__content" data-kicker="Recipes">
                    <div class="block__title">
                        <span><?php echo get_the_title($posts[1]->ID); ?></span>
                    </div>
                    <div class="block__byline"><?php the_field('by_line', $posts[1]->ID); ?></div>
                </div>
            </a>
            <?php
                endif;
                if( isset($posts[2]) && !empty($posts[2]) ):
            ?>
            <a id="block_2-0" class="comp block" rel="" data-doc-id="" href="<?php echo get_permalink($posts[2]->ID); ?>">
                <div class="block__media">
                    <div class="img-placeholder" style="padding-bottom:66.6%;">
                        <?php echo get_the_post_thumbnail($posts[2]->ID, 'large', ['class' => 'block__img']); ?>
                    </div>
                </div>
                <div class="block__content" data-kicker="Running">
                    <div class="block__title">
                        <span><?php echo get_the_title($posts[2]->ID); ?></span>
                    </div>
                    <div class="block__byline"><?php the_field('by_line', $posts[2]->ID); ?></div>
                </div>
            </a>
            <?php
                endif;
            ?>
        </div>
    </div>
</section>
<?php endif; ?>