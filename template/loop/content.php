<li class="g-item block-list-item">
    <a id="block_<?php the_ID(); ?>" class="comp block--small block" href="<?php the_permalink();?>">
        <div class="block__media">
            <div class="img-placeholder" style="padding-bottom:66.6%;">
                <?php echo get_the_post_thumbnail(get_the_ID(), 'large', ['class'=>'block__img']); ?>
            </div>
        </div>
        <div class="block__content" data-kicker="Beginners">
            <div class="block__title">
                <span><?php the_title() ?></span>
            </div>
            <div class="block__byline"> <?php the_field('by_line');?></div>
        </div>
    </a>
</li>