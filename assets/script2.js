(function ($) {
    var e = $("html"),
        l = $("body"),
        o = $(".header"),
        n = $(".header-nav-panel"),
        i = $(".main"),
        a = $(".footer"),
        s = $(".menu-button, .search-button");

    s.on("click", function(s) {
        s.preventDefault(),
        $(this).hasClass("search-button") && (o.find(".general-search-input").focus(),
        n.scrollTop(0)),
        o.toggleClass("is-fullnav"),
        i.toggleClass("is-invisible"),
        a.toggleClass("is-invisible"),
        e.toggleClass("is-noscroll"),
        l.toggleClass("is-noscroll"),
        0 < $(window).scrollTop() ? o.addClass("header-condensed") : o.removeClass("header-condensed");
    });

    $(".trusted-links").on("click", function() {
        o.removeClass("is-fullnav"),
        i.removeClass("is-invisible"),
        a.removeClass("is-invisible"),
        e.removeClass("is-noscroll"),
        l.removeClass("is-noscroll");
    });
}(jQuery));