		</main>
		<footer id="footer_1-0" class="comp footer" role="contentinfo" data-tracking-container="true">
			<div class="footer-inner">
				<div class="footer-social">
					<div id="newsletter-signup-vue_1-0" class="comp newsletter-signup-vue nl-loaded">
						<form class="newsletter-signup-vue__form">
							<span class="newsletter-signup-vue__header">Daily Healthy Eating Tips to Your Inbox</span>
							<p class="newsletter-signup-vue__subheader"></p>
							<div class="newsletter-signup-vue__wrapper">
								<div class="newsletter-signup-vue__input-wrapper input-group"><input type="email" name="email" placeholder="Enter your email" required="required" aria-label="Enter your email" class="mntl-newsletter-submit__input mntl-newsletter-submit__button newsletter-signup-vue__input"> <button type="submit" class="btn newsletter-signup-vue__button">Sign Up</button></div>
								<span class="newsletter-signup-vue__header newsletter-signup-vue__header--success" style="display: none;">You're in!</span>
								<p class="success-message" style="display: none;">Thank you, , for signing up.</p>
								<p class="error-message" style="display: none;">There was an error. Please try again.</p>
							</div>
						</form>
					</div>
					<div id="social-follow_1-0" class="comp extended social-follow static-social-follow mntl-block" data-tracking-container="true">
						<div id="social-follow__title_1-0" class="comp social-follow__title mntl-text-block">
							Follow Us
						</div>
						<div id="social-follow__intro_1-0" class="comp social-follow__intro mntl-text-block"></div>
						<ul id="social-follow__list_1-0" class="comp social-follow__list mntl-block">
							<li id="social-follow__item_1-0" class="comp social-follow__item social-follow__btn mntl-block">
								<a href="//www.facebook.com/verywell" target="_blank" rel="noopener" id="social-follow__link_1-0" class=" social-follow__link mntl-text-link social-follow__link--facebook" data-tracking-container="true">
									<span id="mntl-text-block_1-0" class="comp is-vishidden mntl-text-block">
									facebook</span>
									<svg class="icon icon-facebook ">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
									</svg>
								</a>
							</li>
							<li id="social-follow__item_1-0-1" class="comp social-follow__item social-follow__btn mntl-block">
								<a href="//www.pinterest.com/verywell" target="_blank" rel="noopener" id="social-follow__link_1-0-1" class=" social-follow__link mntl-text-link social-follow__link--pinterest" data-tracking-container="true">
									<span id="mntl-text-block_1-0-1" class="comp is-vishidden mntl-text-block">
									pinterest</span>
									<svg class="icon icon-pinterest ">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
									</svg>
								</a>
							</li>
							<li id="social-follow__item_1-0-2" class="comp social-follow__item social-follow__btn mntl-block">
								<a href="//instagram.com/verywell" target="_blank" rel="noopener" id="social-follow__link_1-0-2" class=" social-follow__link mntl-text-link social-follow__link--instagram" data-tracking-container="true">
									<span id="mntl-text-block_1-0-2" class="comp is-vishidden mntl-text-block">
									instagram</span>
									<svg class="icon icon-instagram ">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-instagram"></use>
									</svg>
								</a>
							</li>
							<li id="social-follow__item_1-0-3" class="comp social-follow__item social-follow__btn mntl-block">
								<a href="//flipboard.com/@verywell" target="_blank" rel="noopener" id="social-follow__link_1-0-3" class=" social-follow__link mntl-text-link social-follow__link--flipboard" data-tracking-container="true">
									<span id="mntl-text-block_1-0-3" class="comp is-vishidden mntl-text-block">
									flipboard</span>
									<svg class="icon icon-flipboard ">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-flipboard"></use>
									</svg>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="loc footer-nav">
					<nav id="tax1-nav_1-0" class="comp tax1-nav mntl-block" role="navigation">
						<ul id="link-list_1-0" class="comp link-list mntl-block">
							<li id="link-list-items_3-0" class="comp link-list-items link-list-item">
								<a href="//www.verywellfit.com/fitness-4156989" class="link-list-link "> Fitness
								</a>
							</li>
							<li id="link-list-items_3-0-1" class="comp link-list-items link-list-item">
								<a href="//www.verywellfit.com/nutrition-4157081" class="link-list-link "> Healthy Eating
								</a>
							</li>
							<li id="link-list-items_3-0-2" class="comp link-list-items link-list-item">
								<a href="//www.verywellfit.com/weight-loss-4157007" class="link-list-link "> Weight Loss
								</a>
							</li>
							<li id="link-list-items_3-0-3" class="comp link-list-items link-list-item">
								<a href="//www.verywellfit.com/news-latest-research-and-trending-topics-4846425" class="link-list-link "> News
								</a>
							</li>
						</ul>
					</nav>
					<ul id="footer-links_1-0" class="comp footer-links">
						<li class="footer-links-item"><a href="//www.verywellfit.com/review-board-4796531" data-component="footerLinks" data-type="medicalReviewBoard" data-ordinal="1" data-source="footerLinks">Our Review Board</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/about-us" data-component="footerLinks" data-type="ourStory" data-ordinal="1" data-source="footerLinks">About Us</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/our-editorial-process-4778011" data-component="footerLinks" data-type="editorialProcess" data-ordinal="1" data-source="footerLinks">Editorial Process</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/anti-racism-and-diversity-pledge-5024836" data-component="footerLinks" data-type="antiRacismPledge" data-ordinal="1" data-source="footerLinks">Anti-Racism Pledge</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/legal-4773966#privacy-policy" data-component="footerLinks" data-type="privacyPolicy" data-ordinal="1" data-source="footerLinks">Privacy Policy</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/in-the-news-4782523" data-component="footerLinks" data-type="inTheNews" data-ordinal="1" data-source="footerLinks">In the News</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/legal-4773966#verywell-cookie-disclosure" data-component="footerLinks" data-type="cookiePolicy" data-ordinal="1" data-source="footerLinks">Cookie Policy</a></li>
						<li class="footer-links-item"><a href="//www.dotdash.com/our-brands/" target="_blank" rel="noopener" data-component="footerLinks" data-source="footerLinks" data-type="advertiseWithUs" data-ordinal="1">Advertise</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/legal-4773966#terms-of-use" data-component="footerLinks" data-type="termsOfUse" data-ordinal="1" data-source="footerLinks">Terms of Use</a></li>
						<li class="footer-links-item"><a href="//www.dotdash.com/careers/" data-component="footerLinks" target="_blank" data-source="footerLinks" data-type="careersAtDotdash" data-ordinal="1" rel="noopener">Careers</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/legal-4773966#california-privacy-notice" data-component="footerLinks" data-type="californiaPrivacyNotice" data-ordinal="1" data-source="footerLinks">California Privacy Notice</a></li>
						<li class="footer-links-item"><a href="//www.verywellfit.com/about-us#ContactUs" data-component="footerLinks" data-type="contact" data-ordinal="1" data-source="footerLinks">Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="loc footer-bottom">
				<div id="copyright_2-0" class="comp copyright mntl-text-block">
					Ⓒ 2020 About, Inc. (Dotdash) — All rights reserved
				</div>
			</div>

		</footer>
		<svg class="is-hidden">
			<defs>
				<symbol id="mntl-sc-block-starrating-icon">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
						<path d="M0,0v20h20V0H0z M14.2,12.2l1.1,6.3l-5.4-3.2l-5.1,3.2l1-6.3L1.4,8l5.9-0.8l2.6-5.8l2.7,5.8L18.5,8L14.2,12.2z"></path>
					</svg>
				</symbol>
			</defs>
		</svg>
		<script type="text/javascript" data-glb-js="bottom" src="<?php echo get_template_directory_uri(); ?>/assets/script2.js" async="" defer="true" data-loaded="loaded"></script>
        <?php wp_footer(); ?>
	</body>
</html>