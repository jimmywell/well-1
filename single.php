<?php
    global $post;
    get_header();
    the_post();
    $top_category_id_3 = n2t_get_top_category(null, 3);
    $top_category_3 = get_category($top_category_id_3);

    $top_category_id_2 = n2t_get_top_category(null, 2);
    $top_category_2 = get_category($top_category_id_2);

    $top_category_id_1 = n2t_get_top_category(null, 1);
    $top_category_1 = get_category($top_category_id_1);

    $level1 = $level2 = false;
?>
    <article class="comp right-rail article">
        <header class="loc article-header article-header">
            <div class="comp article-preheading mntl-block">
                <span class="comp news-badge-wrapper mntl-block"></span>
                <div class="comp taxlevel-2 breadcrumbs">
                    <?php if($top_category_id_3): ?>
                    <div class="breadcrumb-container">
                        <a href="<?php echo get_category_link($top_category_id_3); ?>" class="breadcrumb-2 breadcrumbs-link"><?php echo $top_category_3->name; ?></a>
                        <svg class="icon icon-arrow-right 2"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg>
                    </div>
                    <?php endif; ?>
                    <?php if($top_category_id_2): $level2 = true; ?>
                    <div class="breadcrumb-container">
                        <a href="<?php echo get_category_link($top_category_id_2); ?>" class="breadcrumb-2 breadcrumbs-link"><?php echo $top_category_2->name; ?></a>
                        <svg class="icon icon-arrow-right 2"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg>
                    </div>
                    <?php endif; ?>
                    <?php if($top_category_id_1): $level1 = true; ?>
                    <div class="breadcrumb-container">
                        <a href="<?php echo get_category_link($top_category_id_1); ?>" class="breadcrumb-2 breadcrumbs-link"><?php echo $top_category_1->name; ?></a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <h1 class="comp article-heading">
                <?php the_title(); ?>
            </h1>
            <h2 id="article-subheading_1-0" class="comp article-subheading">Calories, Carbs, and Health Benefits of Wakame</h2>
            <div class="comp not-news article-meta mntl-block">
                <div class="comp article-meta__wrapper mntl-block">
                    <div class="comp article-byline mntl-byline mntl-block">
                        <span class="comp mntl-byline__text mntl-text-block">By</span>
                        <div class="comp mntl-byline__name mntl-block mntl-dynamic-tooltip--trigger">
                            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class=" mntl-byline__link mntl-text-link">
                                <span class="link__wrapper"><?php the_author(); ?></span>
                            </a>
                        </div>
                    </div>
                    <span class="comp byline-article-updated mntl-text-block">
                        Published on <?php the_date('F d, Y') ?>
                    </span>
                </div>
            </div>
        </header><!-- Header Post Meta  -->
        <div class="comp journey-nav-wrapper mntl-block"></div>
        <figure id="figure-article_1-0" class="comp figure-landscape right-rail__offset lock-journey figure-article mntl-block" data-tracking-container="true">
            <div id="figure-article__media_1-0" class="comp figure-article__media mntl-block">
                <div class="img-placeholder" style="padding-bottom:69.2%;">
                    <?php echo get_the_post_thumbnail(get_the_ID(), 'full', ['class'=>'figure-article__image mntl-primary-image']); ?>
                </div>
            </div>
            <figcaption id="figure-article__caption_1-0" class="comp figure-article__caption mntl-figure-caption figure-article-caption">
                <span class="figure-article-caption-owner">
                    <p>Patrik Giardino/Getty Images&nbsp;</p>
                </span>
            </figcaption>
        </figure><!-- Post Featured Image -->
        <div class="comp breadcrumbs-list">
            <div class="breadcrumbs-list-header">
                <a href="#fitness-trends-overview-4581861" class="breadcrumbs-list-image">
                    <img src="https://www.verywellfit.com/thmb/ZQuONNsfZWgzr9X3ZeKtQg2VD3o=/220x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/Illo_FitnessTrends-c1511cf6d575473e8ed4989e363e945c.jpg" alt="Fitness Trends and Alternative Workouts">
                </a>
                <span class="breadcrumbs-list-subtitle">
                    <a href="<?php echo get_category_link($top_category_3); ?>">
                        More in <?php echo $top_category_3->name;  ?>
                        <svg class="breadcrumbs-list-icon icon-empty-caret">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
                        </svg>
                    </a>
                </span>
            </div>
            <ul class="breadcrumbs-list-list">
                <li class="breadcrumbs-list-item">
                    <a href="#crosstraining-overview-4581867" data-ordinal="1" class="breadcrumbs-list-link">
                        Cross-Training
                        <svg class="breadcrumbs-list-icon icon-empty-caret">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-empty-caret"></use>
                        </svg>
                    </a>
                </li>
            </ul>
        </div><!-- Breadcrumb Post -->
        <div id="list-sc_1-0" class="comp article-content expert-content list lock-journey no-commerce list-sc mntl-block">
            <?php the_content(); ?>
        </div><!-- Post Content -->
    </article>
    <?php
        $related = get_posts(
            array(
                'category__in' => wp_get_post_categories( $post->ID ),
                'numberposts'  => 5,
                'post__not_in' => array( $post->ID )
            )
        );
        if( $related ):
    ?>
    <div id="prefooter_1-0" class="comp prefooter mntl-block">
        <div id="prefooter-content_1-0" class="comp prefooter-content mntl-block">
            <section id="related-article-list_2-0" class="comp related-article-list article-list">
                <span class="section-title">Related Articles</span>
                <div class="loc content section-body">
                    <ul id="block-list_2-0" class="comp g g-four-up block-list" data-chunk="">
                        <?php
                            foreach( $related as $post ) {
                                setup_postdata($post);
                                get_template_part('template/loop/content');
                            }
                            wp_reset_postdata();
                        ?>
                    </ul>
                </div>
            </section>
        </div>
    </div><!-- Related Posts -->
    <?php endif; ?>
<?php get_footer(); ?>
